# Gatsby Aerobatic Starter Blog
Gatsby starter for creating a blog, enhanced with package.json pre-configured for
[Aerobatic](https://www.aerobatic.com) static web site hosting.

Install this starter (assuming Gatsby is installed) by running from your CLI:
`gatsby new gatsby-blog https://bitbucket.org/aerobatic/gatsby-demo.git`

If that errors, you may need to `npm -g i gatsby` first.

## Running in development
`gatsby develop`

## Hosting and Continuous Deployment
The following Aerobatic config is pre-configured in package.json:

```javascript
"_aerobatic": {
  "build": {
    "engine": "npm",
    "script": "build-prod",
    "output": "public"
  }
}
```

When you are setting up Aerobatic hosting make sure to specify `public` as the deploy directory.

![Aerobatic Config Screenshot](https://www.aerobatic.com/media/blog/create-gatsby-site.png)
