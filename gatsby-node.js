import fs from 'fs-extra'
import _includes from 'lodash/fp/includes'

function buildLunrSearchIndex (pages) {
  // remove pages that should not be indexed and store in new array
  const cleanedPages = pages.filter((page) => {
    const removes = ['_template.js', 'index.js', '404.md']
    // return pages not in removes
    return ! _includes(page.file.basename, removes)
  })

  console.log(`Creating search index for ${cleanedPages.length} pages`)

  // create searchData JSON
  const searchData = cleanedPages.map((page) =>
    Object.assign({},
      {
        [page.path]: {
          title: page.data.title,
          content: fs.readFileSync(`pages/${page.file.path}`, 'utf8'),
          url: page.path,
        },
      })
  )

  // create search_data.json file
  fs.outputJSONSync('public/search-data.json', searchData)
}

exports.postBuild = function gatsbyNodePostBuild (pages, callback) {
  buildLunrSearchIndex(pages)
  callback()
}
